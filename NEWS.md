# r4urep [dev](https://forgemia.inra.fr/urep/dev_utils/r_utils/r4urep/-/releases/dev) (??/??/????)

- Correct nullModelsStatistics function (significance calculation)
- Change weighted moments computation by using matrix calculation

# r4urep [1.0](https://forgemia.inra.fr/urep/dev_utils/r_utils/r4urep/-/releases/1.0) (14/12/2022)

- First package release
