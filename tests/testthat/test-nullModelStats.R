testthat::test_that("statistics of distribitution for null model", {
  testthat::expect_equal(
    object = r4urep::nullModelDistributionStatistics(observedValue = 2,
                                                     randomValues = c(1, 4, 5, 6, 8),
                                                     significanceThreshold = c(0.025, 0.975)),
    expected = list(-1.081734, -1.352168, 1.159001, FALSE),
    tolerance = 0.0001)
})
